import { game, scale, tileDimensions } from "../../appearance/game.js";
import { renderExercise } from "../../index.js"
import {IsGameEnded} from "../../appearance/alertScreen";
import {bodyEnd, bodyStart, sendRequest} from "../../Request";
import {Timer} from "../Timer";
import {Mechanism} from "./Mechanism";
import {Chain} from "./Chain";


export function addAuthentificationButtonToStage(infoButton, retryButton, text) {
    const authentificationButton = new PIXI.Sprite.from('./assets/authentificate.png');
    authentificationButton.anchor.set(0.5);
    authentificationButton.scale.set(scale);
    authentificationButton.x = tileDimensions * 5;
    authentificationButton.y = tileDimensions * 2;
    game.stage.addChild(authentificationButton);

    authentificationButton.interactive = true;
    authentificationButton.buttonMode = true;


    return authentificationButton;
}