import { tileDimensions, scale, game } from "../../appearance/game.js";
import {RuleBlockContainer} from "../../index.js";
import {Timer} from "../Timer.js";
import {centerSprite} from "../../mechanics/helpers.js";
import {MutationBlock} from "../Block.js";
import {gameWidth} from "../../appearance/game.js";
import {RuleBlock} from "../Block.js";
import {Goal} from "../../index.js";
import {GoalClass} from "../Block.js";
import {removeSprite} from "../../mechanics/helpers.js";
import {ruleBlocksContainerY} from "../../appearance/constantPositions.js";
import {gameHeight} from "../../appearance/game.js";
import {ruleBlocksWrapper} from "../../appearance/game.js";
import {Mechanism} from "./Mechanism.js";
import {Chain} from "../static/Chain.js";
import {createTextSprite} from "../text.js";
import {bodyEnd, bodyStart, sendRequest} from "../../Request";
import {PlaceHolder} from "./PlaceHolder";
import {Hearts} from "../Hearts";
import {makeBordersAroundAlertScreen} from "../../appearance/renderBg";
import {numOfBorderTilesWidth} from "../../appearance/game";
import {mainTextStyle} from "../text";
import {IsGameEnded} from "../../appearance/alertScreen";


function makeTip(arrowX, arrowY, arrowRotation, msg, flag, callBackFunction, ...args) {
    const arrow = new PIXI.Sprite.from('./assets/info-arrow.png');
    arrow.scale.set(scale*1.8);
    arrow.anchor.set(0.5);
    arrow.x = arrowX;
    arrow.y = arrowY;
    arrow.rotation = arrowRotation;
    let pos = true;
    arrow.interval = setInterval(() => {
        if (arrowRotation === 0 || arrowRotation === -Math.PI) {
            if (arrow.x > arrowX + tileDimensions / 2 || arrow.x < arrowX - tileDimensions / 2) pos = !pos;
            if (pos) arrow.x += 0.1;
            else arrow.x -= 0.1;
        }
        else {
            if (arrow.y > arrowY + tileDimensions / 2 || arrow.y < arrowY - tileDimensions / 2) pos = !pos;
            if (pos) arrow.y += 0.1;
            else arrow.y -= 0.1;
        }
    }, 1);
    game.stage.addChild(arrow);

    let graphics = new PIXI.Graphics();
    graphics.beginFill(0x414044);
    graphics.lineStyle(2, 0xFFFFFF);
    graphics.drawRect(0, 0, 400, 200);
    graphics.scale.set(scale * 1.25);
    graphics.x = gameWidth / 2 - 200 * scale * 1.25;
    game.stage.addChild(graphics);


    let smallBox = new PIXI.Graphics();
    smallBox.beginFill(0x414044);
    smallBox.lineStyle(2, 0xFFFFFF);
    smallBox.drawRect(0, 0, 125, 25);
    smallBox.x = 200 - 125 / 2;
    smallBox.y = 165;
    graphics.addChild(smallBox);

    const commentTextStyle = new PIXI.TextStyle(mainTextStyle);
    commentTextStyle.wordWrap = true;
    commentTextStyle.wordWrapWidth = 380;
    commentTextStyle.fontSize /= scale;
    commentTextStyle.align = 'center';
    const text = createTextSprite(msg, commentTextStyle);
    text.resolution = 2;
    text.anchor.set(0.5);
    text.x = 200;
    text.y = 80;
    graphics.addChild(text);


    const buttonTextStyle = new PIXI.TextStyle(commentTextStyle);
    buttonTextStyle.fontSize /= 1.5;
    const nextBtn = createTextSprite('CONTINUE', buttonTextStyle);
    nextBtn.resolution = 2;
    nextBtn.interactive = true;
    nextBtn.buttonMode = true;
    nextBtn.anchor.set(0.5);
    nextBtn.x = 200;
    nextBtn.y = 180;
    graphics.addChild(nextBtn);


    if(flag) {
        graphics.y = 4 * tileDimensions;
    }
    else {
        graphics.y = gameHeight - 12 * tileDimensions;
    }

    game.stage.children.forEach(child => child.alpha = 0.1);
    text.alpha = 1;
    nextBtn.alpha = 1;
    smallBox.alpha = 1;
    arrow.alpha = 1;
    graphics.alpha = 1;

    for (let i = 0; i < args.length; ++i) {
        args[i].alpha = 1;
    }


    sendRequest("math_game_log",
        bodyStart() +
        ",\"action\":\"help\"" +
        ",\"comment\":\"" + msg +"\"" +
        bodyEnd()
    );

    nextBtn.on("pointerup", () => {
        [arrow, text, nextBtn, graphics, smallBox].forEach(obj => removeSprite(obj));
        callBackFunction();
    });
}

export function addInfoButtonToStage(startingBlock, goalBlock, retryButton, authButton) {
    const infoButton = new PIXI.Sprite.from('./assets/info.png');
    infoButton.anchor.set(0.5);
    infoButton.scale.set(scale);
    infoButton.x = tileDimensions * 2;
    infoButton.y = tileDimensions * 2;
    game.stage.addChild(infoButton);

    setTimeout(() => {
    infoButton.interactive = true;
    infoButton.buttonMode = true;}, 2500);

    infoButton.on("pointerup", () => {
        Timer.stop();
        RuleBlock.allRuleBlocksArr.forEach(ruleBlock => {
            ruleBlock.stop();
        });
        RuleBlock.isDraggingPossible = false;
        RuleBlockContainer.children.forEach(ruleBlock => {
            ruleBlock.stop();
        });
        Mechanism.await();
        Mechanism.visible = false;
        PlaceHolder.visible = false;
        Chain.stop();

        infoButton.interactive = false;
        infoButton.buttonMode = false;
        authButton.interactive = false;
        authButton.buttonMode = false;
        retryButton.interactive = false;
        retryButton.buttonMode = false;

        let lastPos;
        if(numOfBorderTilesWidth > 28) {
            lastPos = Timer.sprite.x - tileDimensions * 3.5 - tileDimensions * Math.ceil(Hearts.sprites.length);
        }
        else {
            lastPos = Timer.sprite.x - tileDimensions * 3.5 - tileDimensions * Math.ceil(Hearts.sprites.length / 2)
        }

        makeTip(
            gameWidth / 2,
            MutationBlock.y - tileDimensions,
            Math.PI / 2,
            "This is your starting expression", true,
            () => {
                makeTip(
                    gameWidth / 2,
                    GoalClass.y - tileDimensions * 3,
                    Math.PI / 2,
                    "You should reach this goal expression by sequence of changes", true,
                    () => {
                        const arrowX = RuleBlock.allRuleBlocksArr[0].x > gameWidth / 2
                            ? gameWidth / 2 : RuleBlock.allRuleBlocksArr[0].x;
                        makeTip(
                            arrowX,
                            ruleBlocksContainerY + tileDimensions * 11,
                            - Math.PI / 2,
                            "Drag these rule blocks and drop them under the mechanism to apply the rule to starting" +
                            " expression", false,
                            () => {
                                makeTip(
                                    lastPos,
                                    tileDimensions * 2,
                                    0,
                                    "Don't run out of time and hearts!", false,
                                    () => {
                                        RuleBlock.allRuleBlocksArr.forEach(ruleBlock => {
                                            ruleBlock.continue()
                                        });
                                        RuleBlock.isDraggingPossible = true;
                                        Timer.continue();
                                        Chain.continue();
                                        Mechanism.continue();
                                        Mechanism.visible = true;
                                        PlaceHolder.visible = true;
                                        infoButton.interactive = true;
                                        infoButton.buttonMode = true;
                                        retryButton.interactive = true;
                                        retryButton.buttonMode = true;
                                        authButton.interactive = true;
                                        authButton.buttonMode = true;
                                        game.stage.children.forEach(child => {
                                            child.alpha = 1;
                                        });
                                    }, ...Hearts.sprites, Timer.sprite
                                )
                            }, RuleBlockContainer
                        )
                    }, goalBlock.mainContainer
                )
            },  startingBlock.sprite

        );
    });
    return infoButton;
}