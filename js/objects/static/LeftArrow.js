import { game, scale, tileDimensions } from "../../appearance/game.js";
import { renderExercise } from "../../index.js"
import {IsGameEnded} from "../../appearance/alertScreen";
import {bodyEnd, bodyStart, sendRequest} from "../../Request";
import {Timer} from "../Timer";
import {Mechanism} from "./Mechanism";
import {Chain} from "./Chain";

export function addLeftArrowToStage(infoButton, retryButton, text) {
    const leftArrow = new PIXI.Sprite.from('./assets/left_arrow.png');
    leftArrow.anchor.set(0.5);
    leftArrow.scale.set(scale);
    leftArrow.x = tileDimensions * 2;
    leftArrow.y = tileDimensions * 2;
    game.stage.addChild(leftArrow);

    leftArrow.interactive = true;
    leftArrow.buttonMode = true;
    leftArrow.visible = false;

    return leftArrow;
}