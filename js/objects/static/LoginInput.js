import { game, scale, tileDimensions } from "../../appearance/game.js";
import {gameWidth, numOfBorderTilesWidth} from "../../appearance/game";

export function addLoginInputToStage(currentLogin) {
    let loginInputWidth = Math.min(700 * scale,  gameWidth - 8*tileDimensions);

    var input = new PIXI.TextInput({
        input: {fontSize: `${0.95 * tileDimensions}px`, width: `${loginInputWidth}px`},
        box: {
            fill: 0xE8E9F3, rounded: 16, stroke: {color: 0xCBCEE0, width: 4}
        }
    });

    input.x = gameWidth / 2 -  loginInputWidth / 2;

    input.y = tileDimensions * 1.35;
    input.placeholder = "Enter your login ...";
    if (currentLogin != null && currentLogin !== ""){
        input.text  = currentLogin;
    }
    game.stage.addChild(input);
    input.focus();
    return input;
}