import {game, gameWidth, tileDimensions, scale, numOfBorderTilesWidth} from "../appearance/game.js";
import {gameHeight} from "../appearance/game";

class HeartsClass
{
    constructor() {
        this.renderHearts = (amountOfHearts) => {
            this.sprites = [];

            for (let i = 0; i < amountOfHearts; i++) {
                const heartSprite = new PIXI.Sprite.from('./assets/heart.png');
                heartSprite.scale.set(scale * 2);
                heartSprite.anchor.set(0.5);

                if(numOfBorderTilesWidth > 28) {
                    heartSprite.x = gameWidth - tileDimensions * 4.5 - i * tileDimensions ;
                    heartSprite.y = tileDimensions * 2;
                }
                else {
                    if(i % 2  == 0)
                    {
                        heartSprite.x = gameWidth - tileDimensions * 4.5 - i*tileDimensions / 2;
                        heartSprite.y = tileDimensions * 2.5;
                    }
                    else {
                        heartSprite.x = gameWidth - tileDimensions * 4.5 - (i - 1) *tileDimensions / 2;
                        heartSprite.y = tileDimensions * 1.5;
                    }
                }


                this.sprites.push(heartSprite);
                game.stage.addChild(heartSprite);
            }

            /*for (let i = 0; i < amountOfHearts; i++) {
                const heartSprite = new PIXI.Sprite.from('./assets/heart.png');
                heartSprite.scale.set(scale * 2);
                heartSprite.anchor.set(0.5);

                heartSprite.x = gameWidth - tileDimensions*2;
                heartSprite.y = gameHeight - 12 *tileDimensions - i*tileDimensions;


                this.sprites.push(heartSprite);
                game.stage.addChild(heartSprite);
            }*/


        };

        this.hide = () => {
                Hearts.sprites.forEach(heart => {
                heart.visible = false;
            });
        };

        this.show = () => {
                Hearts.sprites.forEach(heart => {
                heart.visible = true;
            });
        };

        this.deleteHeart = () => game.stage.removeChild(Hearts.sprites.pop());

        this.heartsLeft = () => this.sprites.length;
    }
}

export const Hearts = new HeartsClass();