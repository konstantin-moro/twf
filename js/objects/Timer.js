import {game, gameWidth, tileDimensions, numOfBorderTilesWidth} from "../appearance/game.js";
import {Hearts} from "./Hearts.js";
import {addAlert} from "../appearance/alertScreen.js";
import {createTextSprite} from "./text.js";
import {bodyEnd, bodyStart, sendRequest} from "../Request";

class TimerClass
{
    constructor() {
        this.start = seconds => {
            this.clear();

            this.seconds = seconds;
            this.secondsOriginal = seconds;
            this.greenLimit = this.seconds;
            this.yellowLimit = this.seconds - this.seconds / 100 * 35;
            this.redLimit = this.seconds - this.seconds / 100 * 70;

            this.textStyle = {
                fontFamily: 'Arial',
                fill: "green",
                fontSize: tileDimensions
            };

            this.intervalFunction = () => {
                this.seconds -= 1;
                if (this.seconds === 0) {
                    sendRequest("math_game_log",
                        bodyStart() +
                        ",\"action\":\"lose\"" +
                        bodyEnd()
                    );
                    addAlert(false);
                }
                if (this.seconds <= this.redLimit) this.sprite.style.fill = "red";
                else if (this.seconds <= this.yellowLimit) this.sprite.style.fill = "yellow";
                else if (this.seconds <= this.greenLimit) this.sprite.style.fill = "green";
                this.sprite.text = `${this.seconds}`;
            };
            this.continue();

            this.sprite = createTextSprite(`${this.seconds}`, this.textStyle);
            this.sprite.anchor.set(0.5);


            if(numOfBorderTilesWidth > 28)
            {
                this.sprite.x = gameWidth - 2.5 * tileDimensions;
            }
            else
            {
                this.sprite.x = gameWidth - 2.5 * tileDimensions;
            }

            this.sprite.y = tileDimensions * 2;
            game.stage.addChild(this.sprite);
        };

        this.getTimeMS = () => {
            if (this.seconds >= 0) {
                return this.seconds * 1000;
            } else {
                return -1;
            }
        };

        this.getInitialTimeMS = () => {
            if (this.secondsOriginal >= 0) {
                return this.secondsOriginal * 1000;
            } else {
                return -1;
            }
        };

        this.stop = () => clearInterval(this.interval);

        this.continue = () => this.interval = setInterval(this.intervalFunction, 1000);

        this.clear = () => {
            this.seconds = null;
            this.stop();
            this.interval = null;
        };

        this.hide = () => this.sprite.visible = false;
    }
}

export const Timer = new TimerClass();