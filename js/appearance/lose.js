import { gameHeight, gameWidth, tileDimensions} from "./game.js";
// import {menu} from "./menu.js";
import { makeObjectStageSwitcher } from "./stageManagement.js";
import {switchStage} from "./stageManagement.js";
import {menu} from "./menu.js";
import {createText} from "../objects/text.js";
import {createTextSprite} from "../objects/text.js";

const loseStage = new PIXI.Application({
    backgroundColor: 0x22303e,
    autoDensity: true,
    resizeTo: window
});

const loseMessage = createTextSprite(`You lost`, createText(gameWidth / 15));
loseMessage.x = gameWidth / 2;
loseMessage.y = gameHeight / 2;
loseMessage.anchor.set(0.5);
loseStage.stage.addChild(loseMessage);

const reloadText = createTextSprite(`Reload this page to try again!`, createText(gameWidth / 25));
reloadText.x = gameWidth / 2;
reloadText.y = loseMessage.y + loseMessage.height + tileDimensions;
reloadText.anchor.set(0.5);
loseStage.stage.addChild(reloadText);


export { loseStage }