import { MutationBlock, RuleBlock} from "../objects/Block.js";
import {gameHeight, tileDimensions} from "./game.js";
import {numOfBorderTilesWidth} from "./game.js";

function generateArrayOfRandomAssets(numOfBlocks, blockType) {
    const res = [];
    let path = "./assets/game_blocks/inca_front-";
    switch (blockType) {
        case "front": path = "./assets/game_blocks/inca_front-"; break;
        case "back": path = "./assets/bg/blocks/inca_back-"; break;
        case "dark": path = "./assets/bg/blocks-dark/block-"; break;
    }
    // there are only 8 assets available
    for (let i = 0; i < numOfBlocks; i++) {
        let randomAssetNum = 0;
        randomAssetNum = Math.floor(Math.random() * Math.floor(8));
        res.push(`${path}${randomAssetNum}.png`);
    }
    return res;
}

function renderBlocks (exercise) {
    exercise.rules.forEach(rule => {
        new RuleBlock(generateArrayOfRandomAssets(), { left: rule.left, right: rule.right})
    });
    new MutationBlock(generateArrayOfRandomAssets(), exercise.startingExpression);
}

function renderBlock(numOfBlocks) {
    new RuleBlock(generateArrayOfRandomAssets(numOfBlocks), {left: "2 + 2", right: 4})
}

export { renderBlocks, renderBlock, generateArrayOfRandomAssets }