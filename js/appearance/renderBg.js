import {game, tileDimensions, numOfBorderTilesHeight, numOfBorderTilesWidth, gameWidth, gameHeight, scale} from "./game.js"

export function addBorderBlock(assetUrl, x, y, rotated=false, container, scale=1) {
    const border = new PIXI.Sprite.from(assetUrl);
    border.x = x;
    border.y = y;
    border.zIndex = 3;
    if (scale) border.scale.set(scale);
    if (rotated) border.rotation = Math.PI / 2;
    container.addChild(border);
}

function addSubject(spriteUrl, x=0, y=0, container, scale=1) {
    const block = new PIXI.Sprite.from(spriteUrl);
    block.x = x;
    block.y = y;
    if (scale) block.scale.set(scale);
    container.addChild(block);
}

export function makeBordersAroundSprite(
    container, numOfBorderTilesWidth, numOfBorderTilesHeight, tileStyle
) {

    for (let i = 0; i < numOfBorderTilesWidth; i++) {
        // top
        addBorderBlock(`./assets/bg/borders/${tileStyle}/tile020.png`, i * tileDimensions, 0, false, container, scale);
        addBorderBlock(`./assets/bg/borders/${tileStyle}/tile021.png`, i * tileDimensions, tileDimensions / 2, false, container, scale);

        //bot
        addBorderBlock(`./assets/bg/borders/${tileStyle}/tile020.png`, i * tileDimensions, numOfBorderTilesHeight * tileDimensions - tileDimensions / 2, false, container, scale);
        addBorderBlock(`./assets/bg/borders/${tileStyle}/tile021.png`, i * tileDimensions, numOfBorderTilesHeight * tileDimensions - tileDimensions, false, container, scale);
    }

    for (let i = 0; i < numOfBorderTilesHeight; i++) {
        // left
        addBorderBlock(`./assets/bg/borders/${tileStyle}/tile020.png`, tileDimensions / 2, i * tileDimensions, true, container, scale);
        addBorderBlock(`./assets/bg/borders/${tileStyle}/tile021.png`, tileDimensions, i * tileDimensions, true, container, scale);

        // right
        addBorderBlock(`./assets/bg/borders/${tileStyle}/tile020.png`, numOfBorderTilesWidth * tileDimensions, i * tileDimensions, true, container, scale);
        addBorderBlock(`./assets/bg/borders/${tileStyle}/tile021.png`, numOfBorderTilesWidth * tileDimensions - tileDimensions / 2, i * tileDimensions, true, container, scale);
    }

    const angleBlock = tileStyle === "back" ? './assets/bg/blocks/inca_back-0.png' : './assets/game_blocks/inca_front-0.png';
    // angles
    addSubject(angleBlock, 0, 0, container, scale);
    addSubject(angleBlock, numOfBorderTilesWidth * tileDimensions - tileDimensions, 0, container, scale);
    addSubject(angleBlock, 0, numOfBorderTilesHeight * tileDimensions - tileDimensions, container, scale);
    addSubject(angleBlock, numOfBorderTilesWidth * tileDimensions - tileDimensions, numOfBorderTilesHeight * tileDimensions - tileDimensions, container, scale);
}

export function makeBordersAroundAlertScreen(
    container, numOfBorderTilesWidth, numOfBorderTilesHeight, dimensions
) {
    const alertTileScale = dimensions / 32;

    for (let i = 0; i < numOfBorderTilesWidth + 1; i++) {
        // top
        addBorderBlock(`./assets/bg/borders/front/tile020.png`, i * dimensions, 0, false, container, alertTileScale);
        addBorderBlock(`./assets/bg/borders/front/tile021.png`, i * dimensions, dimensions / 2, false, container, alertTileScale);

        //bot
        addBorderBlock(`./assets/bg/borders/front/tile020.png`, i * dimensions, numOfBorderTilesHeight * dimensions - dimensions / 2, false, container, alertTileScale);
        addBorderBlock(`./assets/bg/borders/front/tile021.png`, i * dimensions, (numOfBorderTilesHeight - 1) * dimensions, false, container, alertTileScale);
    }

    for (let i = 0; i < numOfBorderTilesHeight; i++) {
        // left
        addBorderBlock(`./assets/bg/borders/front/tile020.png`, dimensions / 2, i * dimensions, true, container, alertTileScale);
        addBorderBlock(`./assets/bg/borders/front/tile021.png`, dimensions, i * dimensions, true, container, alertTileScale);

        // right
        addBorderBlock(`./assets/bg/borders/front/tile020.png`, (numOfBorderTilesWidth + 1) * dimensions , i * dimensions, true, container, alertTileScale);
        addBorderBlock(`./assets/bg/borders/front/tile021.png`, (numOfBorderTilesWidth + 1) * dimensions - dimensions / 2, i * dimensions, true, container, alertTileScale);
    }

    const angleBlock = './assets/game_blocks/inca_front-0.png';
    // angles
    addSubject(angleBlock, 0, 0, container, alertTileScale);
    addSubject(angleBlock, numOfBorderTilesWidth * dimensions, 0, container, alertTileScale);
    addSubject(angleBlock, 0, (numOfBorderTilesHeight - 1) * dimensions, container, alertTileScale);
    addSubject(angleBlock, numOfBorderTilesWidth * dimensions, numOfBorderTilesHeight * dimensions - dimensions, container, alertTileScale);
}

function renderBg() {

    makeBordersAroundSprite(game.stage, numOfBorderTilesWidth, numOfBorderTilesHeight, "back");
    // random blocks
    // addSubject('./assets/bg/blocks/inca_back-6.png', tileDimensions, gameHeight - 4 * tileDimensions, 3 * scale);
    // addSubject('./assets/bg/blocks/inca_back-7.png', gameWidth - 4 * tileDimensions, gameHeight - 4 * tileDimensions, 3 * scale);

    // add big bg blocks
    addSubject('./assets/bg/blocks/inca_back-0.png', gameWidth - 3 * tileDimensions, gameHeight - 11 * tileDimensions, game.stage, 2 * scale);
    addSubject('./assets/bg/blocks/inca_back-1.png', tileDimensions, gameHeight - 11 * tileDimensions, game.stage,2 * scale);

    // columns
    addSubject('./assets/bg/columns/column2.png', tileDimensions, gameHeight - 5 * tileDimensions, game.stage,4 * scale);
    addSubject('./assets/bg/columns/column1.png', tileDimensions, gameHeight - 9 * tileDimensions, game.stage,4 * scale);

    addSubject('./assets/bg/columns/column2.png', gameWidth - 3 * tileDimensions, gameHeight - 5 * tileDimensions, game.stage,4 * scale);
    addSubject('./assets/bg/columns/column1.png', gameWidth - 3 * tileDimensions, gameHeight - 9 * tileDimensions, game.stage,4 * scale);
}

export { renderBg }