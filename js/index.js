import { renderBg } from "./appearance/renderBg.js";
import {MutationBlock, RuleBlock} from "./objects/Block.js";
import {game, gameWidth, gameHeight, tileDimensions, numOfBorderTilesWidth} from "./appearance/game.js";
import {Hearts} from "./objects/Hearts.js";
import {PlaceHolder} from "./objects/static/PlaceHolder.js";
import { Mechanism } from "./objects/static/Mechanism.js";
import {Timer} from "./objects/Timer.js";
import {ruleBlocksContainerY} from "./appearance/constantPositions.js";
import MathOperations from "./mechanics/MathOperations.js";
import {Steps} from "./objects/Steps.js";
import {ExerciseSettings} from "./objects/ExerciseSettings.js";
import {GoalClass} from "./objects/Block.js";
import { Level } from "./Level.js";
import { sendRequest, bodyStart, bodyEnd } from "./Request.js";
import {shuffleArray} from "./mechanics/helpers.js";
import { addInfoButtonToStage} from "./objects/static/Info.js";
import {addRetryButtonToStage} from "./objects/static/Retry.js";
import {addLoginInputToStage} from "./objects/static/LoginInput";
import {addAuthentificationButtonToStage} from "./objects/static/Authentificate";
import {addLeftArrowToStage} from "./objects/static/LeftArrow";
import {clearContainer} from "./mechanics/helpers.js";
import {Chain} from "./objects/static/Chain.js";
import {addAlert} from "./appearance/alertScreen.js";
import {mainTextStyle} from "./objects/text.js";
import {createTextSprite} from "./objects/text.js";
import {IsGameEnded} from "./appearance/alertScreen";

let Goal = null;
let login = "";

let ua = new DeviceUUID().parse();
let uuid = new DeviceUUID().get();
let hardwareProperties = ua.os + "-" + ua.cpuCores + "-" + ua.resolution + "-" + ua.platform + "-" + ua.browser + "-" + ua.version + "-" + ua.language + "-touch_" + ua.isTouchScreen;


function rand(min, max){
    return (Math.random() * (max - min)) + min
}

let totalTimeMultCoef = rand(0.5, 1.5);
let totalAwardMultCoef = rand(0.5, 1.5);


function getLogin(){
    return login;
}

function getUUID(){
    return uuid;
}

function getHardwareProperties(){
    return hardwareProperties;
}

function getSpeedCoef(){
    return ua.cpuCores;
}

function getTimeMultCoef(){
    return totalTimeMultCoef;
}

function getAwardMultCoef(){
    return totalAwardMultCoef;
}

const textStyle = new PIXI.TextStyle(mainTextStyle);

function authenticateUser(user, password)
{
    var token = user + ":" + password;

    // Should i be encoding this value????? does it matter???
    // Base64 Encoding -> btoa
    var hash = btoa(token);

    return "Basic " + hash;
}

// Dynamic exercise creation
function renderExercise() {
    clearContainer(game.stage);
    MutationBlock.resetMutationBlock();
    RuleBlock.resetRuleBlock();
    GoalClass.resetGoal();
    Chain.stop();

    setTimeout(() => {
        let text;

        if(numOfBorderTilesWidth > 28) {
            text = createTextSprite(`Current level: ${Level.currentIndex + 1}/${Level.levels.length}`, textStyle);
        }
        else {
            text = createTextSprite(`LVL: ${Level.currentIndex + 1}/${Level.levels.length}`, textStyle);
        }

        text.resolution = 2;
        text.x = tileDimensions * 6;
        text.y = tileDimensions * 1.5;
        game.stage.addChild(text);
        const level = Level.levels[Level.currentIndex];
        MutationBlock.y = gameHeight - tileDimensions * 2;
        let startingBlock = new MutationBlock(level.startingExpression);
        setTimeout(() => Timer.start(Math.floor(level.time * getTimeMultCoef())), 2000);
        // Add default game objects
        Goal = new GoalClass(level.goalExpression, level.steps, level.scope);
        // addMovingChainToStage();
        Chain.addToStage();
        let retryButton = addRetryButtonToStage();
        let leftArrow = addLeftArrowToStage();

        // game.stage.addChild(new RuleBlock({left: "2 + 2", right: "4"}).sprite);
        PlaceHolder.addToStage();
        Mechanism.addToStage();
        Steps.resetSteps();
        Steps.setLimit(level.steps);
        Hearts.renderHearts(level.hearts);
        ExerciseSettings.setScope(level.scope);
        IsGameEnded.isGameEnded = false;

        var inputForm = addLoginInputToStage(login);
        inputForm.visible = false;
        inputForm.on('blur', () => {
            login = inputForm.text;
            sendRequest("math_game_log",
                bodyStart() +
                ",\"action\":\"sign\"" +
                bodyEnd()
            );
        });

        sendRequest("math_game_log",
            bodyStart() +
            ",\"action\":\"start\"" +
            bodyEnd()
        );

        var authButton = addAuthentificationButtonToStage();
        let infoButton = addInfoButtonToStage(startingBlock, Goal, retryButton, authButton);

        authButton.on("pointerup", () => {
            infoButton.visible = false;
            retryButton.visible = false;
            authButton.visible = false;
            text.visible = false;
            Hearts.hide();
            inputForm.visible = true;
            leftArrow.visible = true;
        });

        leftArrow.on("pointerup", () => {
            infoButton.visible = true;
            retryButton.visible = true;
            authButton.visible = true;
            text.visible = true;
            Hearts.show();
            inputForm.visible = false;
            leftArrow.visible = false;
        });

//        console.log("start infiniteLoop");
        infiniteLoop(level);

        renderBg();
    }, 100);
}


function makeArrayOfAppropriateRules(optimalRules, attendantRules, expression) {
    return optimalRules.concat(attendantRules).filter(rule => MathOperations.isRuleApplicable(expression, rule.left, rule.right));
}

export let RuleBlockContainer = null;

function infiniteLoop(exercise, lastChildWidth=null) {
    const currentMutation = MutationBlock.topExpression;
    let arrayOfAppropriateRules = makeArrayOfAppropriateRules(exercise.optimalRules, exercise.attendantRules, currentMutation);

    // fix if array empty to prevent game crash
    if (arrayOfAppropriateRules.length <= 1) {
        while (arrayOfAppropriateRules.length !== 5) {
            arrayOfAppropriateRules.push(exercise.attendantRules[Math.floor(Math.random() * exercise.attendantRules.length)]);
        }
    }

    shuffleArray(arrayOfAppropriateRules);

    RuleBlockContainer = new PIXI.Container();
    RuleBlockContainer.y = ruleBlocksContainerY;
    game.stage.addChild(RuleBlockContainer);
    let i = 0;
    // fixing first block wrong positioning
    const firstBlock = new RuleBlock(arrayOfAppropriateRules[0]).sprite;
    firstBlock.children[0].y = RuleBlock.lowered
        ? RuleBlockContainer.y + tileDimensions * 5
        : RuleBlockContainer.y + tileDimensions;
    RuleBlockContainer.addChild(firstBlock);
    RuleBlockContainer.x -= firstBlock.width / 2;
//    console.log ("first generation; x ", firstBlock.x, "; y ", firstBlock.y, "; container x ", RuleBlockContainer.x, "; y ", RuleBlockContainer.y);
    i++;

    if (lastChildWidth) firstBlock.x -= lastChildWidth;

    const interval = setInterval(() => {
        const lastChild = RuleBlockContainer.children[RuleBlockContainer.children.length - 1];
        // recursion
        if (IsGameEnded.isGameEnded === true){
//            console.log("Game ended!");
            clearInterval(interval);
        } else {
            if (MutationBlock.topExpression !== currentMutation) {
                clearInterval(interval);
//                console.log("recurse infiniteLoop");
                infiniteLoop(exercise, lastChild.width);
            } else {
                if (RuleBlockContainer.children.length === 0) {
                    const block = new RuleBlock(arrayOfAppropriateRules[0]).sprite;
//                    console.log("empty generation; x ", block.x, "; y ", block.y);
                    RuleBlockContainer.addChild(block);
                    i++;
                    if (i >= arrayOfAppropriateRules.length) i = 0;
                } else {
                    if (lastChild.x >= lastChild.width / 2) {
                        const newBlock = new RuleBlock(arrayOfAppropriateRules[i]).sprite;
                        newBlock.x -= newBlock.width / 2;
                        newBlock.x += tileDimensions * 2;
//                        console.log("internal generation ", lastChild.x, " ", lastChild.width / 2, "; x ", newBlock.x, "; y ", newBlock.y);
                        RuleBlockContainer.addChild(newBlock);
                        i++;
                        if (i >= arrayOfAppropriateRules.length) i = 0;
                    }
                }
            }
        }
    }, 1);
}

document.body.appendChild(game.view);
renderExercise();

export {renderExercise, getLogin, getUUID, getHardwareProperties, getSpeedCoef, getTimeMultCoef, getAwardMultCoef, Goal};