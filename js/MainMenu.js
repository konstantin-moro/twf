import {createTextSprite, mainTextStyle} from "./objects/text";
import {Level} from "./Level";
import {game, gameWidth, movingSpeed, scale, tileDimensions} from "./appearance/game";
import {Timer} from "./objects/Timer";
import {Chain, createChainForBlock} from "./objects/static/Chain";
import {renderBg} from "./appearance/renderBg";
import {generateArrayOfRandomAssets} from "./appearance/renderGamingBlocks";
import {ruleBlocksContainerY} from "./appearance/constantPositions";
import {renderExercise} from "./index";
import {removeSprite} from "./mechanics/helpers";


const textStyle = new PIXI.TextStyle(mainTextStyle);
const numberOfLevelsOnOnePage = 10;
let MenuBlockContainer = null;
const baseMenuBlockMoveSpeedMultiplier = 10;

function createMenuBlock(container, expressionSprite, numOfBlocks, blockStyle, scaleCoefficient, alpha=null) {
    let urls = generateArrayOfRandomAssets(numOfBlocks, blockStyle);
    for (let i = 0; i < urls.length; i++) {
        const sprite = new PIXI.Sprite.from(urls[i]);
        sprite.scale.set(scaleCoefficient * scale);
        sprite.x = i * scaleCoefficient * tileDimensions;
        sprite.y = 0;
        if (alpha) sprite.alpha = alpha;
        container.addChild(sprite);
    }

    container.addChild(expressionSprite);
    expressionSprite.anchor.set(0.5);
    expressionSprite.x = numOfBlocks * scaleCoefficient * tileDimensions / 2;
    expressionSprite.y = tileDimensions;

    container.pivot.x = numOfBlocks * scaleCoefficient * tileDimensions / 2;
    container.pivot.y = tileDimensions * scaleCoefficient / 2;

    return container;
}

class MenuBlock {
    static lowered = false;
    static allMenuBlocksArr = [];
    static moveSpeedMultiplier = baseMenuBlockMoveSpeedMultiplier;

    static resetMenuBlock() {
        MenuBlock.lowered = false;
    }
    constructor(name, blockFunction) {
        MenuBlock.lowered = !MenuBlock.lowered;

        this.sprite = new PIXI.Container();
        this.sprite.sortableChildren = true;

        const exp = createTextSprite(name, textStyle);
        const numOfBlocks = Math.ceil(exp.width / (2 * tileDimensions)) >= 3
            ? Math.ceil(exp.width / (2 * tileDimensions))
            : 3;
        this.block = createMenuBlock(
            new PIXI.Container(),
            exp,
            numOfBlocks,
            "front",
            2
        );

        this.block.interactive = true;
        this.block.buttonMode = true;
        this.block.zIndex = 2;
        this.block
            .on("pointerup", blockFunction);

        this.chain = createChainForBlock(MenuBlock.lowered);
        this.block.y += this.chain.height;

        this.sprite.addChild(this.block);
        this.sprite.addChild(this.chain);


        this.intervalFunction = () => {
            this.sprite.x += movingSpeed * MenuBlock.moveSpeedMultiplier;
            if (this.sprite.x > gameWidth + numOfBlocks * tileDimensions * 2) {
                //clearInterval(this.sprite.interval);
                //removeSprite(this.sprite);
            }
        };

        this.sprite.stop = () => clearInterval(this.sprite.interval);

        this.sprite.continue = () => this.sprite.interval = setInterval(this.intervalFunction, 1);

        this.sprite.interval = this.sprite.continue();
        MenuBlock.allMenuBlocksArr.push(this.sprite);
    }
}

function addBlocksToMainMenu(arrayOfButtonsNames, arrayOfButtonsFunctions, reverse, koef=1.5, lower=false) {
    MenuBlock.moveSpeedMultiplier = baseMenuBlockMoveSpeedMultiplier;
    if (reverse === true) {
        MenuBlock.moveSpeedMultiplier = -baseMenuBlockMoveSpeedMultiplier;
        arrayOfButtonsFunctions.reverse();
        arrayOfButtonsNames.reverse();
    }
    MenuBlockContainer = new PIXI.Container();
    MenuBlockContainer.y = ruleBlocksContainerY;
    game.stage.addChild(MenuBlockContainer);
    let i = 0;
    const firstBlock = new MenuBlock(arrayOfButtonsNames[i], arrayOfButtonsFunctions[i]).sprite;
    firstBlock.children[0].y = MenuBlock.lowered
        ? MenuBlockContainer.y + tileDimensions * 5
        : MenuBlockContainer.y + tileDimensions;
    if (reverse ===true) {
        firstBlock.x = gameWidth;
    }
    MenuBlockContainer.addChild(firstBlock);
    MenuBlockContainer.x -= firstBlock.width / 2;
    i++;
    const interval = setInterval(() => {
        if (i === arrayOfButtonsNames.length){
            const lastChild = MenuBlockContainer.children[MenuBlockContainer.children.length - 1];
            const firstChild = MenuBlockContainer.children[0];
            if ((lastChild.x + firstChild.x >= gameWidth && reverse === false) ||
                (lastChild.x + firstChild.x <= gameWidth && reverse === true)){
                MenuBlock.moveSpeedMultiplier = 0;
                Chain.stop();
                clearInterval(interval);
            }
        }
        else {
            const lastChild = MenuBlockContainer.children[MenuBlockContainer.children.length - 1];
            if ((lastChild.x >= koef * lastChild.width && reverse === false) ||
                (lastChild.x <=  gameWidth - koef * lastChild.width  && reverse === true)) {
                if (lower === false) {
                    MenuBlock.resetMenuBlock();
                }
                const newBlock = new MenuBlock(arrayOfButtonsNames[i], arrayOfButtonsFunctions[i]).sprite;
                if (reverse === false) {
                    newBlock.x -= newBlock.width / 2;
                    newBlock.x += tileDimensions * 2;
                }
                else {
                    newBlock.x += gameWidth + newBlock.width / 2;
                    newBlock.x -= tileDimensions * 2;
                }
                MenuBlockContainer.addChild(newBlock);
                i++;
            }
        }
    }, 1);
}

function removeAllBlocks(nextAction, reverse) {
    MenuBlock.moveSpeedMultiplier = baseMenuBlockMoveSpeedMultiplier;
    if(reverse){
        MenuBlock.moveSpeedMultiplier = -baseMenuBlockMoveSpeedMultiplier;
    }
    const interval = setInterval(() => {
        let lastChild = MenuBlockContainer.children[0];
        MenuBlockContainer.children.forEach(child => {
            if ((child.x > lastChild.x && reverse === true)||
                (child.x < lastChild.x && reverse === false)){
                lastChild = child;
            }
        });
        if ((lastChild.x > gameWidth + lastChild.width) || (lastChild.x < -lastChild.width)){
            MenuBlock.allMenuBlocksArr.forEach(x => {removeSprite(x)});
            MenuBlock.allMenuBlocksArr = [];
            removeSprite(MenuBlockContainer);
            MenuBlockContainer = null;
            nextAction();
            clearInterval(interval);
        }
    }, 1);
}

function  addMainMenuBlocks(reverse=false) {
    MenuBlock.resetMenuBlock();
    let arrayOfButtonsNames = ["Exit", "Rules", "Settings", "Play"];
    let arrayOfButtonsFunctions = [
        () => nextPage(0),
        () => nextPage(0),
        () => nextPage(0),
        () => nextPage(0)
    ];

    addBlocksToMainMenu(arrayOfButtonsNames, arrayOfButtonsFunctions, reverse);
}

function nextPage(lastLevelIndex) {
    if (lastLevelIndex !== Level.levels.length) {
        removeAllBlocks( () => {
            addLevelsBlockToMainMenu(
                lastLevelIndex + 1,
                Math.min(lastLevelIndex + numberOfLevelsOnOnePage, Level.levels.length),
                false);
        }, false);
    }
}


function startGame(levelId, reverse) {
    removeAllBlocks(() => {
        Level.currentIndex = levelId - 1;
        renderExercise();
    }, reverse);
}

function previousPage(firstLevelIndex) {
    removeAllBlocks( () => {
        if (firstLevelIndex !== 1) {
            addLevelsBlockToMainMenu(
                firstLevelIndex - numberOfLevelsOnOnePage,
                firstLevelIndex - 1,
                true);
        }
        else{
            addMainMenuBlocks(true);
        }
    }, true);
}

function addLevelsBlockToMainMenu(firstLevelIndex, lastLevelIndex, reverse) {
    console.log(firstLevelIndex, lastLevelIndex);
    let arrayOfButtonsNames = ["Next Page"];
    let arrayOfButtonsFunctions = [() => nextPage(lastLevelIndex)];
    for(let i = lastLevelIndex; i >= firstLevelIndex; i--){
        arrayOfButtonsNames.push(`Level${i}`);
        arrayOfButtonsFunctions.push(() => startGame(i, reverse));
    }
    arrayOfButtonsNames.push("Previous Page");
    arrayOfButtonsFunctions.push(() => previousPage(firstLevelIndex));
    addBlocksToMainMenu(arrayOfButtonsNames, arrayOfButtonsFunctions, reverse, 0.5, true);
}


export function addMainMenuToStage() {
    renderBg();
    const text = createTextSprite(`Main menu`, textStyle);
    text.resolution = 2;
    text.x = tileDimensions * 5;
    text.y = tileDimensions * 1.5;
    game.stage.addChild(text);
    Timer.clear();
    Chain.addToStage();
    addMainMenuBlocks(false);
}